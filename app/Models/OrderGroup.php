<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderGroup extends Model
{
    protected $fillable=['user_id','authority','code',
        'paid','total_price','total_discount','ref_id' , 'orderGroupStatus',
        'send_method','pay_from_credit'];

    public function orders(){
        return $this->hasMany(Order::class,'order_group_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function discounts(){
        return $this->morphMany(Discount::class, 'discountable');
    }
}
