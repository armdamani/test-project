<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'order_group_id'
        , 'product_id', 'count', 'completing', 'total_price','discount'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'voucher_id');
    }
    public function discounts(){
        return $this->morphMany(Discount::class, 'discountable');
    }
    protected static function bootActionableOrder()
    {
        self::deleting(function ($model) {
            $model->discounts()->delete();
        });
    }

}
