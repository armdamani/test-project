<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;
    protected $fillable = [
        'code','name','description','uses','max_uses','max_uses_user',
        'discount_amount','is_fixed','starts_at','expires_at','type','product_id'
    ];
    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }
}
