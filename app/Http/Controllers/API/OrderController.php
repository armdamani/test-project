<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\VoucherResource;
use App\Models\Discount;
use App\Models\Order;
use App\Models\OrderGroup;
use App\Models\Product;
use App\Models\User;
use App\Models\Voucher;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private function getVoucher($request)
    {
        $request = new VoucherResource($request);
        return $request->toArray($request);
    }

    private function productValidator($id)
    {
        $request = new Request(['id' => $id]);
        $request = $this->validate($request, [
            'id' => 'required|integer',
        ]);
        $product = Product::find($request['id']);
        if ($product) {
            return $product;
        } else {
            return false;
        }
    }

    public function addSingleProductToOrder($id)
    {
        $product = $this->productValidator($id);
        if (!$product) {
            return response('product not found', 404);
        }
        $orders = auth('api')->user()->unCompleteOrders();
        $order = $orders->where('product_id', '=', $product->id)->first();
        if ($order) {
            if ($order->product->id == $product->id) {
                if ($order->voucher_id) {
                    $voucher = Voucher::find($order->voucher_id);
                    $order->count = $order->count + 1;
                    if ($voucher->uses < $voucher->max_uses) {
                        $voucher->uses += 1;
                        $voucher->save();
                        $order->total_price = $order->count * ($product->price - $voucher->discount_amount);
                        $order->total_discount = $order->count * $voucher->discount_amount;
                        $order->save();
                        return response(['order' => $order, 'message' => 'added one count'], 200);
                    } else {
                        $order->count = $order->count + 1;
                        $order->total_price += $product->price;
                        $order->save();
                        return response(['order' => $order, 'message' => '(max use of this discount exceeded):product added without discount'], 200);
                    }

                } else {
                    $order->count = $order->count + 1;
                    $order->total_price += $product->price;
                    $order->save();
                    return response(['order' => $order, 'message' => 'added one count'], 200);
                }

            }
        } else {
            $order = new Order();
            $order->user_id = auth('api')->user()->id;
            $order->product_id = $product->id;
            $order->total_price = $product->price;
            $order->save();
            return response(['order' => $order, 'message' => 'added order!'], 200);
        }
    }

    private function calculateTotalPrice($orders)
    {
        return $orders->sum(function ($order) {
            return $order->total_price;
        });
    }

    public function cart()
    {
        $orders = auth('api')->user()->unCompleteOrders->where('completing', false);
        if ($orders->isNotEmpty()) {
            $totalPrice = $this->calculateTotalPrice($orders);
            $result['orders'] = $orders;
            $result[$totalPrice] = $totalPrice;
            return response(['result' => $result]);
        } else {
            return response("you don't have any orders", 404);
        }

    }

    public function removeSingleProduct($id)
    {
        $product = $this->productValidator($id);
        if (!$product) {
            return response('product not found', 404);
        }
        $order = auth('api')->user()->unCompleteOrders()->where('product_id', '=', $product['id'])->first();
        //only if order is not complete!
        if (!$order) {
            return response(['message' => "You haven't ordered this product"], 404);
        } else {
            if ($order['completing'] == null) {
                //remove order in cart page
                $voucher = $order->voucher()->first();
                if ($voucher) {
                    $voucher_uses = $order->total_discount / $voucher->discount_amount;
                    if ($order['count'] > 1) {
                        if ($order->count <= $voucher_uses) {
                            $voucher->uses--;
                            $voucher->save();
                            $order->total_price -= ($product['price'] - $voucher->discount_amount);
                            $order->total_discount = $voucher->discount_amount;
                            $order->count--;
                            $order->save();
                            return response(['message' => 'you have ' . $order->count . ' items of this product!']);
                        }
                        $order->count = $order->count - 1;
                        $order->total_price -= $product['price'];
                        $order->save();
                        return response(['message' => 'product that didn\'t have discount removed from this order now you have' . ' ' . $order->count . '.'], 200);

                    } else {
                        $voucher->uses--;
                        $voucher->save();
                        $order->discounts()->delete('user_id', auth('api')->user()->id);
                        $order->delete();
                        return response(['message' => 'You don\'t have this product in your order list anymore']);
                    }
                } else {
                    if ($order['count'] > 1) {
                        $order->count = $order->count - 1;
                        $order->save();
                        return response(['message' => 'you have ' . $order->count . ' items of this product!']);
                    } else {
                        $order->delete();
                        return response(['message' => 'You don\'t have this product in your order list anymore']);
                    }
                }
            }
        }

    }

    public function removeCompleteOrder($id)
    {
        $product = $this->productValidator($id);
        $order = auth('api')->user()->unCompleteOrders()->where('product_id', '=', $product['id'])->first();
        if ($order) {
            if ($order->user->id == auth('api')->user()->id) {
                if (!$order->completing) {
                    $voucher = $order->voucher()->first();
                    if ($voucher) {
                        $voucher_uses = $order->total_discount / $voucher->discount_amount;
                        $voucher->uses -= $voucher_uses;
                        $voucher->save();
                        $order->discounts()->delete('user_id', auth('api')->user()->id);
                    }
                    $order->delete();
                    return response(['message' => 'whole order deleted successfully']);
                } else {
                    return response(['message' => "You can't remove complete order"], 401);
                }
            } else {
                return response(['message' => "User can't perform this action."], 401);
            }
        } else {
            return response(['message' => "You havn't this product in your orders"], 404);
        }
    }

    public function getPaymentAuthority($money, User $user)
    {
//        اتصال به درگاه بانکی
        if ($money > 0) {
            $user->credit = 0;
            $user->save();
            return true;
        } else {
            $user->credit = -$money;
            $user->save();
            return true;
        }

    }

    private function CheckOrderGroupDiscountStatusCode()
    {

    }

    private function checkOrderGroupDiscountCode($totalPrice, $voucher)
    {
        if ($voucher) {
            $discount = $this->CheckOrderGroupDiscount($totalPrice, $voucher);
            if ($discount->getStatusCode() == 200) {
                $discount = $discount->getOriginalContent();
                $totalPrice -= $voucher->discount_amount;
                return ['discount' => $discount, 'totalPrice' => $totalPrice];
            } else {
                return $discount;
            }
        } else {
            return response('discount code not found', 404);
        }

    }

    private function checkSendMethod($send_method_code,$totalPrice){
        if ($send_method_code == 1) {
            $totalPrice = $totalPrice + 5000;
            return $totalPrice;
        } else if ($send_method_code == 2) {
            $totalPrice = $totalPrice + 10000;
            return $totalPrice;
        }else{
            return false;
        }
    }

    public function pay(Request $request)
    {
        $this->validate($request,[
            'send_method'=>'required|integer',
            'pay_from_credit'=>'boolean'
        ]);
        $user = auth('api')->user();
        $totalPrice = $this->calculateTotalPrice($user->unCompleteOrders);
        if (!$totalPrice > 0) {
            return response('You have no order', 404);
        }
        if ($request->code) {
            $voucher = $request->code;
            $discount = $this->checkOrderGroupDiscountCode($totalPrice, $voucher);
            if (is_array($discount)) {
                $totalPrice = $discount['totalPrice'];
                $voucher = $discount['discount']->voucher;
                $discount = $discount['discount'];
            } else {
                return $discount;
            }
        } else {
            $voucher = false;
        }

        if ($request->send_method) {
            $totalPrice = $this->checkSendMethod($request->send_method,$totalPrice);
            if(!$totalPrice){
                return response('send method not correct', 400);
            }
        } else {
            return response('send method not defined', 400);
        }
        if ($request->pay_from_credit) {
            $diffrence = $totalPrice - $user->credit;
            $orderGroup = new OrderGroup();
            $orderGroup->user_id = $user->id;
            $orderGroup->authority = $this->getPaymentAuthority($diffrence, $user);
            $orderGroup->code = 0;
            $orderGroup->send_method = $request->send_method;
            $orderGroup->paid = 1;
            $orderGroup->pay_from_credit = 1;
            $orderGroup->total_discount = 0;
            $orderGroup->save();
            if ($voucher) {
                $orderGroup->voucher_id = $voucher->id;
                $voucher->uses++;
                $voucher->save();
                $orderGroup->discounts()->save($discount);
                $orderGroup->total_discount = $voucher->discount_amount;
                $orderGroup->total_price = $totalPrice;
            } else {
                $orderGroup->total_price = $totalPrice;
            }
            $orderGroup->code = $user->id . rand(10000, 99999) . $orderGroup->id;
            $orderGroup->save();
        } else {
            $orderGroup = new OrderGroup();
            $orderGroup->user_id = $user->id;
            $orderGroup->authority = $this->getPaymentAuthority($totalPrice, $user);
            $orderGroup->total_price = $totalPrice;
            $orderGroup->code = 0;
            $orderGroup->send_method = $request->send_method;
            $orderGroup->paid = 1;
            $orderGroup->save();
            $orderGroup->code = $user->id . rand(10000, 99999) . $orderGroup->id;
            $orderGroup->save();

        }
        foreach ($user->unCompleteOrders as $order) {
            $order->order_group_id = $orderGroup->id;
            $order->completing = true;
            if ($voucher) {
                $order->voucher_id = $voucher->id;
            }
            $order->save();
        }
        return response('payment seccessful', 200);
    }

    public function CheckOrderDiscount(Voucher $voucher, Product $product, Order $order)
    {
        if ($voucher->type == 1) {
            $voucherAvailableUses = $voucher->max_uses - $voucher->uses;
            if ($voucherAvailableUses > 0) {
                if ($voucher->product_id == $product->id) {
                    if ($order->completing == 0 && $order->voucher_id) {
                        return response('you used this discount', 400);
                    } else {
                        try {
                            $discount = new Discount();
                            $discount->voucher_id = $voucher->id;
                            $discount->user_id = auth('api')->user()->id;
                            $order->discounts()->save($discount);
                            if ($order->count <= $voucherAvailableUses) {
                                $order->voucher_id = $voucher->id;
                                $order->total_price -= $order->count * $voucher->discount_amount;
                                $order->total_discount = $order->count * $voucher->discount_amount;
                                $voucher->uses += $order->count;
                                $voucher->save();
                                $order->save();
                                return response('voucher applied to your order completely', 200);
                            } else {
                                $order->voucher_id = $voucher->id;
                                $order->total_price -= $voucherAvailableUses * $voucher->discount_amount;
                                $order->total_discount = $voucherAvailableUses * $voucher->discount_amount;
                                $voucher->uses += $voucherAvailableUses;
                                $voucher->save();
                                $order->save();
                                return response('voucher applied to ' . $voucherAvailableUses . ' items of your order!', 200);
                            }

                        } catch (\Exception $exception) {
                            return response('you have used this voucher', 400);
                        }
                    }
                } else {
                    return response('this voucher is for ' . $voucher->product()->first()->name, 400);
                }
            } else {
                return response(['order' => $order, 'message' => 'max use of this discount exceeded'], 200);
            }
        } else {
            return response('this voucher is for isn\'t for product order', 400);
        }

    }

    public function OrderDiscount(Request $request)
    {
        $product = Product::find($request->product_id);
        if ($product) {
            $order = auth('api')->user()->unCompleteOrders()->where('product_id', '=', $product['id'])->first();
            $voucher = $this->getVoucher($request);
            if ($order) {
                if ($voucher) {
                    return $this->CheckOrderDiscount($voucher, $product, $order);
                } else {
                    return response('discount code not found', 404);
                }
            } else {
                return response('order not found', 404);
            }
        } else {
            return response('product not found', 404);
        }
    }


    private function CheckOrderGroupDiscount($totalPrice, $voucher)
    {
        $voucher = Voucher::where('code', '=', $voucher)->first();
        $user = auth('api')->user();
        if ($voucher) {
            if (!$voucher->type) {
                if ($voucher->uses < $voucher->max_uses) {
                    if ($voucher->min_total_price <= $totalPrice) {
                        $discount = Discount::where('user_id', '=', $user->id)->where('voucher_id', '=', $voucher->id)->first();
                        if (!$discount) {
                            $discount = new Discount();
                            $discount->voucher_id = $voucher->id;
                            $discount->user_id = $user->id;
                            return response($discount, 200);
                        } else {
                            return response('you have used this voucher', 400);
                        }
                    } else {
                        return response('min total price for this discount is ' . $voucher->min_total_price, 400);
                    }
                } else {
                    return response('max use for this discount exceeded', 400);
                }
            } else {
                return response('this voucher isn\'t for cart', 400);
            }
        }else{
            return response('voucher not found', 404);
        }
    }

}
