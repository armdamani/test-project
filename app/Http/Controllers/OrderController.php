<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use http\Client\Curl\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function addToCart(Product $product,User $user)
    {
        if (!$product) {
            abort(404);
        }
        $orders = $user->orders->where('completing', false);
        foreach ($orders as $order) {
            if ($order->product->id == $product->id) {
                $order->count = $order->count + 1;
                $order->save();
                return 'added one count';
            }
        }
        $order = new Order();
        $order->user_id = $user->id;
        $order->product_id = $product->id;
        $order->save();
        return 'added order!';
    }

    public function cart(User $user)
    {
        $orders = $user->orders->where('completing', false);
        $totalPrice = $orders->sum(function ($order) {
            return $order->product()->price * $order->count;
        });
        $result['orders'] = $orders;
        $result[$totalPrice] = $totalPrice;
        return $result;

    }

}
