<?php

namespace App\Http\Resources;

use App\Models\Discount;
use App\Models\Voucher;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Resources\Json\JsonResource;

class VoucherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        try {
            $voucher =  Voucher::where('code','=',$request->code)->firstOrFail();
             return $voucher;
        } catch (ModelNotFoundException $e) {
            report($e);
            return false;
        }

    }
}
