<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        try {
            Order::findOrFail($request->id);
            return [
                'id' => $this->id,
                'user_id' => $this->user_id,
                'product_id' => $this->product_id,
                'count' => $this->count,
                'completing'=>$this->completing
            ];
        } catch (ModelNotFoundException $e) {
            report($e);
            return false;
        }
    }
}
