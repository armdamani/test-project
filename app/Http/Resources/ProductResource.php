<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        try {
            $product =Product::findOrFail($request->id);
            return [
                'id' => $product->id,
                'price' => $product->price,
            ];
        } catch (ModelNotFoundException $e) {
            report($e);
            return false;
        }

    }
}
