<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\OrderController;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/register', [AuthController::class,'register']);
Route::post('/login', [AuthController::class,'login'])->name('login');
Route::group(['middleware'=>['auth:api']],function (){
    Route::post('add-to-cart/{id}', [OrderController::class,'addSingleProductToOrder']);
    Route::get('cart', [OrderController::class,'cart'])->name('cart');
    Route::post('remove-product/{id}', [OrderController::class,'removeSingleProduct'])->name('remove_product');
    Route::post('remove-order/{id}', [OrderController::class,'removeCompleteOrder'])->name('remove_order');
    Route::post('pay', [OrderController::class,'pay']);
    Route::post('discount-order',[OrderController::class,'OrderDiscount']);


});



