<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class VoucherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vouchers')->insert([
            'code' => 'hello_basalam',
            'name' =>'basalam-voucher',
            'max_uses' => 10,
            'discount_amount'=>5000,
            'min_total_price'=>100000
        ]);
        DB::table('vouchers')->insert([
            'code' => 'first_product',
            'name' =>'first product voucher',
            'max_uses' => 10,
            'discount_amount'=>2000,
            'product_id'=>1,
            'type'=>1,
        ]);
    }
}
