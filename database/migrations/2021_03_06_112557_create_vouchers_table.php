<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->id();
            // The voucher code
            $table->string( 'code' )->nullable( );

            //product id voucher
            $table->unsignedBigInteger('product_id')->nullable()->comment('شناسه محصول (فقط برای سفارش)');

            // The human readable voucher code name
            $table->string( 'name' );

            // The description of the voucher - Not necessary
            $table->text( 'description' )->nullable( );

            // The number of uses currently
            $table->integer( 'uses' )->unsigned( )->nullable( );

            // The max uses this voucher has
            $table->integer( 'max_uses' )->unsigned()->nullable( );

            //order voucher or order group voucher(default)
            $table->boolean('type')->default(0)->comment('0 for order group and 1 for order');

            // The amount to discount.
            $table->integer( 'discount_amount' );

            //minimum total price
            $table->integer('min_total_price')->nullable()->comment('just for order groups');

            // When the voucher begins
            $table->timestamp( 'starts_at' );

            // When the voucher ends
            $table->timestamp( 'expires_at' )->nullable();

            $table->foreign('product_id')->references('id')->on('products')->onUpdate('restrict')->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
