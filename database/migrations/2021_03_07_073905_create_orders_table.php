<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->comment('شناسه خریدار');
            $table->unsignedBigInteger('order_group_id')->nullable()->comment('شناسه سفارش نهایی شده');
            $table->unsignedBigInteger('product_id')->comment('شناسه محصول');
            $table->unsignedBigInteger('voucher_id')->nullable()->comment('شناسه کد تخفیف');
            $table->boolean('completing')->default(false)->comment('آیا سفارش کامل شده است ؟');
            $table->unsignedInteger('count')->default(1)->comment('تعداد محصول');
            $table->bigInteger('total_price')->nullable()->comment('قیمت کل در زمان خرید');
            $table->bigInteger('total_discount')->default(0)->comment('تخفیف کلی در زمان خرید');
            $table->foreign('order_group_id')->references('id')->on('order_groups')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('voucher_id')->references('id')->on('vouchers')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
