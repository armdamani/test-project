<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_groups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->comment('شناسه سفارش دهنده');
            $table->unsignedBigInteger('voucher_id')->nullable()->comment('شناسه کد تخفیف');
            $table->unsignedBigInteger('code')->comment('کد سفارش');
            $table->string('authority')->comment('چک کننده پرداخت');
            $table->string('ref_id')->comment(' شماره ارجاع پرداخت')->nullable();
            $table->boolean('paid')->default(false)->comment('وضیعت پرداخت');
            $table->boolean('pay_from_credit')->default(false)->comment('آیا از اعتبار پرداخت شده است؟');
            $table->bigInteger('total_price')->default(0)->comment('قیمت کل در زمان خرید');
            $table->bigInteger('total_discount')->default(0)->comment('تخفیف کلی (کد تخفیف ) در زمان خرید');
            $table->integer('orderGroupStatus')->default(1)->comment('وضعیت سفارش : 1-در انتظار پرداخت ۲-در حال پردازش ۳- تحویل داده شده ۴- مرجوعی 5-لغو شده');
            $table->tinyInteger('send_method')->nullable()->comment('۱-با پست ۲-با پیک');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('voucher_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_groups');
    }
}
