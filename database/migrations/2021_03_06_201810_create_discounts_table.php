<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->comment('شناسه کاربر');
            $table->unsignedBigInteger('voucher_id')->comment('شناسه کد تخفیف');
            $table->unsignedBigInteger('discountable_id');
            $table->string('discountable_type');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('voucher_id')->references('id')->on('vouchers')
                ->onDelete('restrict')->onUpdate('restrict');
            $table->primary(['user_id','voucher_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
